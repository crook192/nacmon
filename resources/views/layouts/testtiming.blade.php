@extends('layouts.app')

@section('content')
<h4>Press the button to begin timer and enable http connectivity</h4>
<button type="submit" class="btn btn-success">Enable access</button>
@endsection