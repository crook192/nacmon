@extends('layouts.app')

@section('content')
<h2 class="text-center">Top Sites Blocked</h2>
<div class="progress">
    <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">facebook</div>
    <div class="progress-bar bg-info" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">twitter</div>
    <div class="progress-bar bg-success" role="progressbar" style="width: 55%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">other</div>
  </div>
  <hr>
<div class="row col-xl-10">
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">site.com</h5>
            <p class="card-text">1000 block instances</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">site.com</h5>
            <p class="card-text">1000 block instances</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">

            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>

</div>
<hr>

<h2 class="text-center">Top Intrusion Detection Events</h2>
<div class="progress">
    <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">10.0.0.1</div>
    <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">172.31.1.1</div>
    <div class="progress-bar bg-danger" role="progressbar" style="width: 55%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">192.168.0.1</div>
  </div>
  <hr>

<div class="row">
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">site.com</h5>
            <p class="card-text">1000 block instances</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">site.com</h5>
            <p class="card-text">1000 block instances</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>
    <div class="card col-md-4 col-xs-6">
        <div class="card-body">
            <h5 class="card-title">Title</h5>
            <p class="card-text">Content</p>
        </div>
    </div>
</div>

@endsection
