@extends('layouts.app')

@section('content')


<div class="container col-md6">
    <h1>MAC Filter</h1>
    <div class="col-md4">
        <h4>MAC filter. Add MAC rules here </h4>
        <!-- debug -->
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
        @endif

        @if(session()->has('success'))
            <p class="alert alert-success">{{ session()->get('success')}}</p>
        @endif

        

        
    </div>
    <!-- TODO: Pass form into controller to forward to Python -->
    <form method="POST" action="/mac">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="form-group">
                    <label for="my-select">Source</label>
                    <input type="text" class="form-control" name="mac" placeholder="00:00:00:00:00:00" value="{{ old('src')}}">
                </div>
            </div>
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
    <button type="delete" class="btn btn-danger">Delete</button>
    </div>
    </form>
</div>

@endsection
