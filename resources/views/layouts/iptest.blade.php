@extends('layouts.app')

@section('content')


<div class="container col-md6">
    <h1>IP Test</h1>
    <div class="col-md4">
        <h4>Add IP rules here. This portal will append iptables rules and delete them as needed. </h4>
        <!-- debug -->
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
        @endif
        
        @if(session()->has('success'))
            <p class="alert alert-success">{{ session()->get('success')}}</p>
        @endif
        @if(session()->has('warn'))
            <p class="alert alert-success">{{ session()->get('warn')}}</p>
        @endif
        
    </div>
    <form method="POST" action="/iptest">
    @csrf
    <div class="container">
        <div class="row">
        <div class="col-sm">
            <div class="form-group">
                <label for="my-select">Chain</label>
                <select id="my-select" class="form-control" name="chain" value="{{ old('chain')}}">
                    <option selected></option>
                    <option>Input</option>
                    <option>Output</option>
                </select>
            </div>
        </div>

        <div class="col-sm">
            <div class="form-group">
                <label for="my-select">Protocol</label>
                <select id="my-select" class="form-control" name="protocol" placeholder="Select..." value="{{ old('protocol')}}">
                    <option selected></option>
                    <option>TCP</option>
                    <option>UDP</option>
                    <option>IP</option>
                </select>
            </div>
        </div>
        <div class="col-sm">
            <div class="form-group">
                <label for="my-select">Source</label>
                <input type="text" class="form-control" name="src" placeholder="10.0.0.0/24" value="{{ old('src')}}">

            </div>
        </div>
        <div class="col-sm-1">
            <div class="form-group">
                <label for="my-select">Port</label>
                <input type="text" class="form-control" name="srcport" placeholder="8080" value="{{ old('srcport')}}">
            </div>
        </div>
        <div class="col-sm">
            <div class="form-group">
                <label for="my-select">Dest</label>
                <input type="text" class="form-control" name="dst" placeholder="10.0.0.0/24 OR 10.0.0.0" value="{{ old('dst')}}">
            </div>
        </div>
        <div class="col-sm-1">
            <div class="form-group">
                <label for="my-select">Port</label>
                <input type="text" class="form-control" name="dstprt" placeholder="8080" value="{{ old('dstprt')}}">
            </div>
        </div>
        <div class="col-sm">
            <div class="form-group">
                <label for="my-select">Action</label>
                <select id="my-select" class="form-control" name="action" placeholder="Select..." value="{{ old('action')}}">
                    <option selected></option>
                    <option>Accept</option>
                    <option>Drop</option>
                    <option>Reject</option>
                </select>
            </div>
        </div>
        <div class="col-sm">
            <div class="form-group">
                <label for="my-select">Delete</label>
                <select id="my-select" class="form-control" name="delete" placeholder="Select..." value="{{ old('delete')}}">
                    <option selected></option>
                    <option>Yes</option>
                    <option>No</option>
                </select>
            </div>
        </div>

    </div>
    <button type="submit" class="btn btn-success">Submit</button>
    </div>
    <hr/>
<div class="container col-md6">
    <h1>Ruleset </h1>
    <p>Reading From: {{App\Http\Controllers\RuleController::getRules()}}</p>
    <div class="card">
        <ul class="list-group list-group-flush">
        @foreach (file(App\Http\Controllers\RuleController::getRules()) as $line )
        <li class="list-group-item">{{ $line }}</li> 
        @endforeach
    </div>
</div>

    </form>
</div>

@endsection
