@extends('layouts.app')

@section('content')

<style>

    //div{
     //   border:1px solid black;
    //}

</style>
<div class="container col-md6">
    <h1>NAC index</h1>
    @if(session()->has('success'))
            <p class="alert alert-success">{{ session()->get('success')}}</p>
        @endif
    <div class="col-md4">
        <h4> Welcome to the Network Access Control system.</h4>
        <p>Welcome to the NAC. Network connectivity is provided once you are logged in. When logged in, you can close this page, but please log out after you are finished. </p>
    </div>
</div>


@endsection
