@extends('layouts.app')

@section('content')
<div class="jumbotron">
    <h1 class="display-4">Blocked </h1>
    <hr class="my-4">
    <p class="lead">This page has been restricted. Please contact the system administrator</p>
  </div>
@endsection