<div>
    <form>
        @csrf
        <div class="container">
            <div class="row">
            <div class="col-sm">
                <div class="form-group">
                    <label for="my-select">Chain</label>
                    <select id="my-select" class="form-control" name="chain">
                        <option selected>Select...</option>
                        <option>Preroute</option>
                        <option>Input</option>
                        <option>Postroute</option>
                    </select>
                </div>
            </div>

            <div class="col-sm">
                <div class="form-group">
                    <label for="my-select">Protocol</label>
                    <select id="my-select" class="form-control" name="proto">
                        <option selected>Select...</option>
                        <option>TCP/IP</option>
                        <option>UDP</option>
                        <option>IP</option>
                    </select>
                </div>
            </div>
            <div class="col-sm">
                <div class="form-group">
                    <label for="my-select">Source</label>
                    <input type="text" class="form-control" name="ip-src" placeholder="10.0.0.0/24">

                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    <label for="my-select">Port</label>
                    <input type="text" class="form-control" name="ip-src-prt" placeholder="8080">
                </div>
            </div>
            <div class="col-sm">
                <div class="form-group">
                    <label for="my-select">Destination</label>
                    <input type="text" class="form-control" name="ip-dst" placeholder="10.0.0.0/24 OR 10.0.0.0">
                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    <label for="my-select">Port</label>
                    <input type="text" class="form-control" name="ip-dst-prt" placeholder="8080">
                </div>
            </div>
            <div class="col-sm">
                <div class="form-group">
                    <label for="my-select">Action</label>
                    <select id="my-select" class="form-control" name="action">
                        <option selected>Select...</option>
                        <option>Accept</option>
                        <option>Drop</option>
                        <option>Reject</option>
                    </select>
                </div>
            </div>
            <div class="col-sm">
                <div class="form-group">
                    <input class="form-check-input" type="checkbox" value="" name="isDelete">
                    <label class="form-check-label" for="flexCheckChecked">
                      Delete
                    </label>
                  </div>
            </div>

        </div>
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="clear" class="btn btn-danger">Clear</button>
        </div>
        </form>
</div>
