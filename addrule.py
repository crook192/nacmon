#! /usr/bin/python3
import os
import sys
import json
import base64

json_access = json.loads(base64.b64decode(sys.argv[1]))

token = json_access['_token']
chain = json_access['chain']
protocol = json_access['protocol']
source = json_access['src']
sourcePort = json_access['srcport']
dest = json_access['dst']
destport = json_access['dstprt']
action = json_access['action']
delete = json_access['delete']


rule = ""
newrule = ""
newrule2 = ""
newrule3 = ""
newrule4 = ""
if delete.upper() == 'NO':
   rule = 'iptables -A ' + chain.upper() + ' -s ' + source
   #print (rule)
elif delete.upper() == 'YES': 
    newrule = 'iptables -D ' + chain.upper() + ' ' + source
if protocol.lower() != 'ip' and protocol:
    newrule4 = " -p " + protocol.lower()
if sourcePort and protocol.lower() != 'ip':
    newrule2 = newrule4 + newrule + ' --sport ' + sourcePort
if destport and protocol.lower() != 'ip':
    newrule3 = newrule2 + ' --dport ' + destport
elif not destport:
    newrule3 = newrule2
print(rule + newrule3 + " -j " + action.upper())

if os.system(rule + newrule3 + " -j " + action.upper()) == 0:
    success = {"success": "true"}
    print(json.dumps(success))
else:
    success = {"success": "false"}
    print(json.dumps(success))

os.system('iptables-save > /etc/iptables.ipv4.nat')

