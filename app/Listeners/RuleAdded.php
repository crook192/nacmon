<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Http\Request;

class RuleAdded
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event. Take request 
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RuleAdded $event, Request $request)
    {
        //$passing = $event->toJson();
        //convert event information to JSON 
        //pass object to python 
        //exec('./addrule.py ', $passing);
    }
}
