<?php

namespace App\Http\Controllers;

use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Filesystem;

class RuleController extends Controller
{
    /**
     * Encodes and sends JSON object to python script. Intercepts POST request from 
     * iptest and validates the form.  
     * 
     * Runs an exec script calling @example addrule.py with a b64 encoding of the JSON object
     * 
     * Sends the IP information from the form to a Python script, and awaits a message
     * back from the script. If the returned json value <code> success</code> is true, then the
     * user is notified a rule has been added. Else, the user is notified that either the rule already exists, or failed. 
     * 
     * 
     * @param $request - request sent from the webpage
     * @param $returned_json - json sent from the python script
     * @return JSON->Success
     * 
     * @see addrule.py
     * 
     * 
     */
    public function sendIp(Request $request)
    {
        request()->validate([
            'chain' => 'required',
        ]);
        //we run the python script
        exec('sudo /var/www/html/NAC/addrule.py ' . base64_encode(json_encode($request->toArray())), $output, $result_code);
        if(empty($output)){
            die("The script failed. " . $result_code . print_r($output));
        }
        $returned = base64_decode($output[0]);
        $returned_json = json_decode($returned);
        if($returned_json->success){
            redirect('/iptest')->with('success', 'The rule has added successfully. ');
        }
        else if(!$returned_json->success){
            redirect('/iptest')->withErrors('The rule did not add successfully');
        }
        else{
            redirect('/iptest')->withErrors('An unknown error occurred.');
        }
        return view('layouts.iptest');
    }
    public static function getRules(){
        $ruleset = '/etc/iptables.ipv4.nat';
        return $ruleset;
    }




    /***
     * Sends a MAC address to Python script
     * 
     * @return output
     */
    public function sendMac(Request $request){
        request()->validate([
            'mac' => 'required',
        ]);
        if(filter_var($request->mac, FILTER_VALIDATE_MAC)){
            exec('sudo iptables -A INPUT -m mac  --mac-source ' . $request->mac, $output, $result_code);
            exec('sudo /var/www/html/NAC/shell_scripts/save_iptables.sh');
            if($result_code == 0){
                return redirect('/mac')->with('success' , "MAC Address added: " . $request->mac);  
            } else
            return redirect('/mac')->withErrors("The rule was not accepted. This rule may already be part of the list. ");  
        } else{
            return redirect('/mac')->withErrors("MAC address is invalid. Please use : notation (00:00:00:00:00:00)");
        }


    }



}
