<?
use Illuminate\Http\Request;

class EncodeB64{
    public function encodeB64Json(Request $request){
        $retVal = base64_encode(json_encode($request->toArray()));
        return $retVal;
    }
}
    
