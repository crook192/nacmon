<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;


class UserController extends Controller
{
    use AuthenticatesUsers;

    public static function checkAuth($username){

    } 

    /***
     * 
     * Takes credentials from request, passes them to a shell script and checks for authorisation. 
     * Once authorised, logs in using the default user (1)
     * 
     * 
     * @return redirect
     */
    public function authenticate(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');
        
                //we make sure the user is not trying to log into root or www-data
                if($username === "root" || $username === "www-data"){
                    exec('logger -s [WARN] - Unauthorised login attempt for user: ' . $username);
                    return redirect('/login')->withErrors('You cannot login as root. This attempt has been logged.');
                }
                
                if(posix_getpwnam($username)){ 
                    exec("sudo ../shell_scripts/isAuth.sh " . $username . " " . $password, $output, $exit_code);
                    //verify the command was successful and true value returned
                    if($exit_code == 0 && $output[0]){ 
                        Auth::loginUsingId(1, false);
                    } else if ($exit_code == 2) {
                        return redirect('/login')->withErrors('The password is incorrect. ');
                        exec('logger -s [INFO] - Failed Login Attempt ' . $username);
                    }
                } else{
                    return redirect('/login')->withErrors('The login attempt failed');
                } 

                if(Auth::check()){
                    exec("sudo ../shell_scripts/allow.sh");
                    exec('logger -s [INFO] - User logged into appliance:  ' . $username);
                    return redirect('/')->with('success', 'Hello, '.  $username);
                } else{
                    return redirect('/login')->withErrors('An unknown error has occurred');
                    exec('logger -s [WARN] - Possible Command Injection Attempt  ' . $username);
                }  

                
                 
    }

    public static function logout(){
        Auth::logout();
        exec("sudo ../shell_scripts/deny.sh");
        exec('logger -s [INFO] - User logged out of appliance. ');
        return redirect('/')->with('success', 'You are now logged out. Goodbye!');
    }


}