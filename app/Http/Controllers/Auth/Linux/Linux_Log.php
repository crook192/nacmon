<?php
namespace App\Http\Controllers\;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

/***
 * Log an unauthorised attempt to log in to either root or WWW-DATA
 */
function logUnauthorisedAttempt($user){
    exec('logger -s Unauthorised User: ' . $user );
}