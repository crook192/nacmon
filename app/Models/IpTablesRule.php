<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IpTablesRule extends Model
{
    protected $attributes = [
        $chain,
        $protocol,
        $src,
        $src_port,
        $dst,
        $dst_port,
        $rule_action, 
        $isDelete
    ];    
}