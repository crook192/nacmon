<?php

use App\Http\Controllers\RuleController;
use App\Http\Controllers\Auth\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.index');
});


Route::get('/stats', function() {
    return view('layouts.stats');
});

Route::get('/iptest', function() {
    return view('layouts.iptest');
    //
})->name('iptest');


Route::get('/phpinfo', function() {
    return view('layouts.phpinfo');
});

Route::get('/blocked', function(){
    return view('layouts.blocked'); 
});

Route::get('/mac', function(){
    return view('layouts.macfilter');
});



Auth::Routes();


/**
 * Bypass login functions and begin writing own controller. 
 * 
 * @Override UserAuth
 */
Route::get('/login', function(){
    if(!Auth::check()){
        return view ('auth.login');
    }
    else{
        return view ('layouts.index');
    }
    
});
Route::get('/logout', function(){
    UserController::logout();
    return view ('layouts.index');
});
Route::post('/login', [UserController::class, 'authenticate'])->name('login');
Route::post('/logout', [UserController::class, 'logout'])->name('logout');
//Route::post('/login', [LoginController::class, 'test']);

Route::post('/iptest', [RuleController::class, 'sendIp']);
Route::post('/mac', [RuleController::class, 'sendMac']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
