#!/bin/bash


#Commands to be uysed to drop connections to wlan0, the current NIC in operation for internet connectivity. 

#delete rules
echo Removing accept rules... 
if iptables -D FORWARD -i wlan0 -o eth0 -m state --state RELATED,ESTABLISHED -j DROP &&
iptables -D FORWARD -i eth0 -o wlan0 -j DROP; 
then
echo OK!
else
echo Error.
exit
fi

#add rules
echo Adding drop rules...
if iptables -A FORWARD -i wlan0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT && iptables -A FORWARD -i eth0 -o wlan0 -j ACCEPT; 
then
	echo Commands executed
else
	echo A command may have failed.
fi 


#
#if [[ $? -eq 0 ]]
#then
#	echo -e "\e[1;31m The command completed successfully \e[0m"
#else
#	echo -e "\e[1;31m An error was detected. \e[0m"
#fi
