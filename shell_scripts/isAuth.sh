#!/bin/bash

if [ "$EUID" -ne 0 ]; then
	echo "You are not running as root. Please run as root."
	exit 1
fi

#error handling
#the lack of user and password will not happen between php as error handling already in place. However, this will still be in place.
if [ -z $1 ] || [ -z $2 ]; then
	echo "Usage: [username] [password]"
	exit 1
elif [ $1 == "root" ]; then
	echo "Root not permitted"
	exit 1
fi

#get salt, hash, generate a hash using the salt and compare
salt=$(grep $1 /etc/shadow | awk -F'$' '{print $3}')
hash=$(grep $1 /etc/shadow | awk -F'$' '{print $4}' | awk -F: '{print $1}')
hash_comparison=$(mkpasswd -m sha-512 $2 $salt | awk -F'$' '{print $4}' | awk -F: '{print $1}')
if [ "$hash_comparison" == "$hash" ]; then
echo true


exit 0
else
echo false
exit 2
fi
